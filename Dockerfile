FROM ruby:2.6.3-alpine3.10

# Add packages needed
RUN apk --update --no-cache add --virtual build-dependencies curl-dev ruby-dev build-base git && \
    gem sources --remove https://rubygems.org/ && \
    gem install bundler --source http://rubygems.org --no-document

# Add Gemfile to app folder
ADD Gemfile /app/  

# Install gems
RUN cd /app/ && \
    bundle install --jobs 3 --without development test && \
    apk del build-dependencies

# Add remaining files from the framework to the app folder
ADD . /app

# Set working directory to be the app folder
WORKDIR /app

# Set up environment variables to be overriden -- we can make their default values something else
ENV  TAGS=''\
     TZ='America/New_York'

# Execute the cucumber tests
CMD bundle exec cucumber -t "$TAGS" -f ReportPortal::Cucumber::Formatter
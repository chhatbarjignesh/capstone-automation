
Given(/^I navigate to page$/) do
  puts 'I am able to navigate to the page'
end

Then(/^I validated that value displayed for "([^"]*)" should be "([^"]*)"$/) do |values, value|
  puts 'Values are correct'
end

And(/^I added all the values$/) do
  puts 'I have added all the values'
end

Then(/^I validated that the value is matching with the value displayed in total balance field$/) do
  fail('product bug balance is not matching')
end

Then(/^I validated that the values are formatted as currencies$/) do
 fail('automation issue error in format method in code')
end

Then(/^I validated that the values are formatted as money$/) do
  fail('automation issue error in format method in code')
 end

Then(/^I validated that each value and total balance is greater than (\d+)$/) do |number|
  fail("system issue browser failed to communicate #{number}")
end
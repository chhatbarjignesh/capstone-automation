

at_exit do
  sleep 5
  desc = "#{ENV['BROWSER']} RUN IN #{ENV['OS']}"
  
  response = JSON.parse(RestClient::Request.execute(method: :get, url: "#{ENV['RP_ENDPOINT']}/#{ENV['RP_PROJECT']}/launch?filter.eq.description=#{ENV['RP_DESCRIPTION']}",
    headers: {
      content_type: :json, accept: :json, Authorization: "Bearer #{ENV['RP_UUID']}"
    }){ |response| (response.code == 200) ? response : raise("Fail to get Launch: #{response}") })
  puts response['content'][0]['id']
  RestClient::Request.execute(method: :put, url: "#{ENV['RP_ENDPOINT']}/#{ENV['RP_PROJECT']}/launch/#{response['content'][0]['id']}/update",
    payload: {
      description: desc,
      attributes: [ { key: 'Browser', value: "#{ENV['BROWSER']}" }, { key: 'OS', value: "#{ENV['OS']}" }, { key: 'Run Date', value: "#{Time.now.strftime("%m-%d-%Y")}" }, {key: 'Test Type', value: "#{ENV['TEST_TYPE']}"}, {key: 'Environment', value: "#{ENV['ENVIRONMENT']}"}, {key: 'Version', value: "#{ENV['VERSION']}"} ],
      mode: 'DEFAULT'
  }.to_json,
  headers: {
    content_type: :json, accept: :json, Authorization: "Bearer #{ENV['RP_UUID']}"
  }){ |response| (response.code == 200) ? response : raise("Fail to update Launch: #{response}") }
end
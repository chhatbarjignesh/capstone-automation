require 'rubygems'
require 'watir'
require 'page-object'
require 'reportportal'
require 'restclient'
require 'json'

@description = (Time.now.to_f * 1000).to_s
ENV['RP_DESCRIPTION'] = @description
World(PageObject::PageFactory)
